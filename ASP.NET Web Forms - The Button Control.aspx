ASP.NET Web Forms - The Button Control

	<html>
	<body>

	<form runat="server">
	<asp:Button id="b1" Text="Submit" runat="server" />
	</form>

	</body>
	</html>

Add a Script

	<script runat="server">
	Sub submit(sender As Object, e As EventArgs)
	lbl1.Text="Your name is " & txt1.Text
	End Sub
	</script>

	<html>
	<body>

	<form runat="server">
	Enter your name:
	<asp:TextBox id="txt1" runat="server" />
	<asp:Button OnClick="submit" Text="Submit" runat="server" />
	<p><asp:Label id="lbl1" runat="server" /></p>
	</form>

	</body>
	</html>