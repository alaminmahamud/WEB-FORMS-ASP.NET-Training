ASP.NET Web Forms - The DataList Control

The DataList control is, like the Repeater control, used to display a repeated list of items that are bound to the control. However, the DataList control adds a table around the data items by default.

Bind a DataSet to a DataList Control

The DataList control is, like the Repeater control, used to display a repeated list of items that are bound to the control. However, the DataList control adds a table around the data items by default. The DataList control may be bound to a database table, an XML file, or another list of items. Here we will show how to bind an XML file to a DataList control.

We will use the following XML file in our examples ("cdcatalog.xml"):

	<catalog>
	<cd>
	<title>Empire Burlesque</title>
	<artist>Bob Dylan</artist>
	<country>USA</country>
	<company>Columbia</company>
	<price>10.90</price>
	<year>1985</year>
	</cd>
	<cd>
	<title>Hide your heart</title>
	<artist>Bonnie Tyler</artist>
	<country>UK</country>
	<company>CBS Records</company>
	<price>9.90</price>
	<year>1988</year>
	</cd>
	<cd>
	<title>Greatest Hits</title>
	<artist>Dolly Parton</artist>
	<country>USA</country>
	<company>RCA</company>
	<price>9.90</price>
	<year>1982</year>
	</cd>
	<cd>
	<title>Still got the blues</title>
	<artist>Gary Moore</artist>
	<country>UK</country>
	<company>Virgin records</company>
	<price>10.20</price>
	<year>1990</year>
	</cd>
	<cd>
	<title>Eros</title>
	<artist>Eros Ramazzotti</artist>
	<country>EU</country>
	<company>BMG</company>
	<price>9.90</price>
	<year>1997</year>
	</cd>
	</catalog>

Using Styles 

	You can also add styles to the DataList control to make the output more fancy

	_Example

	<script runat ="server">
		//
	</script>

	<asp:DataList
		 		  id = "cdcatalog" 
		 		  runat = "server"
		 		  cellspacing = "2"
		 		  cellpadding = "2"
		 		  borderstyle = "inset"
		 		  backcolor   = "#e8e8e8"
		 		  width	      = "100%"
		 		  headerstyle-font-name = "Verdana"
		 		  headerstyle-font-size = "12pt"
		 		  headerstyle-font-bold = true
		 		  headerstyle-horizontalalign = "center"
		 		  itemstyle-backcolor = "#778899"
		 		  itemstyle-forecolor = "#ffffff"
		 		  footerstyle-font-size = "9pt"
		 		  footerstyle-font-italic = "true"
	>
		.......
	</asp:DataList>

	