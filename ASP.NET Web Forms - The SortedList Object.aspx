ASP.NET Web Forms - The SortedList Object

The sortedList Object combines the features of both the ArrayList object and the Hashtable Object

The SortedList Object

	<script runat = "server">
		sub Page_Load
			if Not Page.IsPostBack then 
				dim mycountries = new SortedList
					mycountries.Add("N", "Norway")
					mycountries.Add("N", "Norway")
					mycountries.Add("N", "Norway")
					mycountries.Add("N", "Norway")
			end if
		end sub
	</script>


Data Binding

A SortedList object may automatically generate the text and values to the following controls:

asp:RadioButtonList
asp:CheckBoxList
asp:DropDownList
asp:Listbox

To bind data to a RadioButtonList control, first create a RadioButtonList control (without any asp:ListItem elements) in an .aspx page:

	rb.DataSource = mycountries
	rb.TrimToSize()
	rb.DataValueField = "Key"
	rb.DataTextField  = "Value"
	rb.DataBind()

sub displayMessage(s As Object, e As EventArgs)
	lbl1.Text = "" 
end sub


