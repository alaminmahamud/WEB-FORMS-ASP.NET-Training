ASP.NET Web Forms - HTML Pages

How Does it Work?

Fundamentally an ASP.NET page is just the same as an HTML page.

An HTML page has the extension .htm. If a browser requests an HTML page from the server, the server sends the page to the browser without any modifications.

An ASP.NET page has the extension .aspx. If a browser requests an ASP.NET page, the server processes any executable code in the page, before the result is sent back to the browser.

The ASP.NET page above does not contain any executable code, so nothing is executed. In the next examples we will add some executable code to the page to demonstrate the difference between static HTML pages and dynamic ASP pages.



Dynamic page in classic ASP

	<html>
	<body bgcolor="yellow">
	<center>
	<h2>Hello W3Schools!</h2>
	<p><%Response.Write(now())%></p>
	</center>
	</body>
	</html>



Dynamic page in classic ASP.NET

	<html>
	<body bgcolor="yellow">
	<center>
	<h2>Hello W3Schools!</h2>
	<p><%Response.Write(now())%></p>
	</center>
	</body>
	</html>

ASP.NET vs Classic ASP

	server controls make ASP.NET more powerful than classic ASP