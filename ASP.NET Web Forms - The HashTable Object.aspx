ASP.NET Web Forms - The HashTable Object

The HashTable Object contains items in key/value pairs

Create A HashTable
	
	<script runat= "server">
		Sub Page_Load 
			if Not Page.IsPostBack then
				dim mycountries = New Hashtable
				mycountries.Add("N", "Norway")
				mycountries.Add("N", "Norway")
				mycountries.Add("N", "Norway")
				mycountries.Add("N", "Norway")
				mycountries.Add("N", "Norway")
			end if
		End Sub
	</script>

Data Binding

rb.DataSource = mycountries
rb.DataValueField = "Key"
rb.DataTextField = "Value"
rb.DataBind()