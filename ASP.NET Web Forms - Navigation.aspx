ASP.NET Web Forms - Navigation

	ASP.NET has built in navigation controls

	WebSite Navigation

	Maintaining the menu of a large web site is difficult and time consuming.

	In ASP.NET the menu can be stored in a file to make it easier to maintain. This file is normally called web.sitemap, and is stored in the root directory of the web.

	In addition, ASP.NET has three new navigation controls:

	Dynamic menus
	TreeViews
	Site Map Path

The SiteMap File
	
	<?xml version="1.0" encoding="ISO-8859-1" ?>
	<siteMap>
	  <siteMapNode title="Home" url="/aspnet/w3home.aspx">
	    <siteMapNode title="Services" url="/aspnet/w3services.aspx">
	      <siteMapNode title="Training" url="/aspnet/w3training.aspx"/>
	      <siteMapNode title="Support" url="/aspnet/w3support.aspx"/>
	    </siteMapNode>
	  </siteMapNode>
	</siteMap>

Note : The Sitemap file must be placed in the root directory of the web and the URL attributes must be relative to the root directory

Dynamic Menu
	
The <asp:menu> control displays a standard navigation menu

Code Example :

	<asp:SiteMapDataSource id="nav1" runat="server" />

	<form runat="server">
	<asp:Menu runat="server" DataSourceId="nav1" />
	</form>


The <asp:SiteMapDataSource> control automatically connects to the default sitemap file (web.sitemap)



	* TreeView
	<asp:TreeView runat = "server" DataSourceId = "nav1">
	</asp:TreeView>

	* SiteMapPath 
	<asp:SiteMapPath runat = "server"/>