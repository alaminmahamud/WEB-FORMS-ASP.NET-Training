ASP.NET Web Forms - HTML Forms

	All server controls must appear within a <form> tag, and the <form> tag must contain the runat="server" attribute.

ASP.NET Web Forms

	All server controls must appear within a <form> tag, and the <form> tag must contain the runat="server" attribute. The runat="server" attribute indicates that the form should be processed on the server. It also indicates that the enclosed controls can be accessed by server scripts:

	<form runat = "server">
		..HTML + server controls
	</form>

	Note: The form is always submitted to the page itself. If you specify an action attribute, it is ignored. If you omit the method attribute, it will be set to method="post" by default. Also, if you do not specify the name and id attributes, they are automatically assigned by ASP.NET.

	Note: An .aspx page can only contain ONE <form runat="server"> control!

If you select view source in an .aspx page containing a form with no name, method, action, or id attribute specified, you will see that ASP.NET has added these attributes to the form. It looks something like this:

	<form name = "_ct10" method = "post" action = "page.aspx" id = "_ct10">
		.. some code
	</form>


Submitting a Form
	
	<!-- Method To run at Server -->
	<script>
		Sub submit(Source As Object, e As EventArgs)
			button1.Text = "You Clicked Me!"
		End Sub
	</script>

	<!DOCTYPE html>
	<html>
		<body>
			<form runat = "server">
				<asp:Button id= "button1" Text = "Click Me!" runat = "server" onClick = "submit"/>
			</form>
		</body>
	</html>