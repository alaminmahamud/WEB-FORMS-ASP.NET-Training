ASP.NET Web Forms - XML File

<?xml version="1.0" encoding="ISO-8859-1"?>

<countries>

<country>
  <text>Norway</text>
  <value>N</value>
</country>

<country>
  <text>Sweden</text>
  <value>S</value>
</country>

<country>
  <text>France</text>
  <value>F</value>
</country>

<country>
  <text>Italy</text>
  <value>I</value>
</country>



Bind a DataSet to a List Control

we need the data namespace to work with dataset objects

<%@ Import Namespace = "System.Data" %>


// nex create a Dataset for the XML file and load the XML file into the DataSet when the page is first loaded

<script runat = "server">
	if Not Page.IsPostBack then 
		dim mycountries = new DataSet
		mycountries.ReadXML(MapPath("countries.xml"))
		rb.DataSource = mycountries
		rb.DataValueField = "value"
		rb.DataTextField = "text"
		rb.DataBind()

	end if
</script>	
</script>
