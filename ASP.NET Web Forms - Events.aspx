ASP.NET Web Forms - Events
	
	An Event Handler is a subroutine that executes code for a given event.


ASP.NET Event Handlers

	Look At the Following Table:

	<%
		lbl1.Text = "The date and time is " & now()
	 %>

	 <html>
	 	<body>
	 		<form runat = "server">
	 			<h3>
	 				<asp:label id="lbl1" runat = "server"/>
	 			</h3>
	 		</form>
	 	</body>
	 </html>

	 When will the code above be executed? The answer is: "You don't know..."

The Page_Load Event
	
	The Page_Load event is one of many events that ASP.NET understands. The Page_Load event is triggered when a page loads, and ASP.NET will automatically call the subroutine Page_Load, and execute the code inside it:

	_Example

	<script runat = "server">
		Sub Page_Load
			lbl1.Text = "The Date and time is "& now()
		End Sub
	</script>

	<html>
		<body>
			<form runat = "server">
				<h3><asp:label id="lbl1" runat="server"/></h3>
			</form>
		</body>
	</html>

	The Page_Load Event Contains no object references or event arguments

The Page.IsPostBack Property
	
	The Page_Load subroutine runs EVERY time the page is loaded. If you want to execute the code in the Page_Load subroutine only the FIRST time the page is loaded, you can use the Page.IsPostBack property. If the Page.IsPostBack property is false, the page is loaded for the first time, if it is true, the page is posted back to the server (i.e. from a button click on a form):

<script runat = "server">
	Sub Page_Load
		if Not Page.IsPostBack then
			lbl1.Text = "The Date and time is " & now()
		end if
	End Sub

	Sub submit (s As Object. e As EventArgs)
		lbl2.Text = "Hello World!";
	End Sub
</script>

<html>
	<body>
		<form runat="server">
			<h3><asp:label id="lbl1" runat="server"/></h3>
			<h3><asp:label id="lbl2" runat="server"</h3>
			<asp:button text="Submit" onclick="submit" runat="server"/>
		</form>
	</body>
</html>
