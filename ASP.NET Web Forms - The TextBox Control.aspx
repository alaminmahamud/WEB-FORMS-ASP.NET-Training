ASP.NET Web Forms - The TextBox Control

	_Example:

	<html>
	<body>

	<form runat = "server">

	A Basic TextBox:
	<asp:TextBox id = "tb1" runat="server"/>
	<br/>

	A Password TextBox:
	<asp:TextBox id = "tb2" TextMode = "password" runat = "server"/>

	A TextBox with text:
	<asp:TextBox id="tb4" Text="Hello World!" runat="server" />
	<br /><br />

	A multiline TextBox:
	<asp:TextBox id="tb3" TextMode="multiline" runat="server" />
	<br /><br />

	A TextBox with height:
	<asp:TextBox id="tb6" rows="5" TextMode="multiline"
	runat="server" />
	<br /><br />

	A TextBox with width:
	<asp:TextBox id="tb5" columns="30" runat="server" />

	</form>
	</body>
	</html>

Add A Script
	
	The contents and settings of a TextBox control may be changed by server scripts when a form is submitted. A form can be submitted by clicking on a button or when a user changes the value in the TextBox control.

	In the following example we declare one TextBox control, one Button control, and one Label control in an .aspx file. When the submit button is triggered, the submit subroutine is executed. The submit subroutine writes a text to the Label control:

	<script runat="server">
	Sub submit(sender As Object, e As EventArgs)
	lbl1.Text="Your name is " & txt1.Text
	End Sub
	</script>

	<html>
	<body>

	<form runat="server">
	Enter your name:
	<asp:TextBox id="txt1" runat="server" />
	<asp:Button OnClick="submit" Text="Submit" runat="server" />
	<p><asp:Label id="lbl1" runat="server" /></p>
	</form>

	</body>
	</html>


	<script runat = "server">
		Sub change(sender As Object, e As EventArgs)
			lbl1.Text = "You changed text to "  & txt1.Text 
		End Sub
	</script>

	<asp:TextBox id="txt1" runat = "server" text = "Hello World!" 
	ontextchanged = "change" autopostback = "true"/>