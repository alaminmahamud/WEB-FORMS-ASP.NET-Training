ASP.NET Web Forms - The Repeater Control

The Repeater Control is used to display a repeated list of items that are bound to the control

Bind A Dataset to a Repeater Control

The Repeater control is used to display a repeated list of items that are bound to the control. The Repeater control may be bound to a database table, an XML file, or another list of items. Here we will show how to bind an XML file to a Repeater control.

<?xml version="1.0" encoding="ISO-8859-1"?>

<catalog>
	<cd>
		<title>Empire Burlesque</title>
		<artirst>Bob Dylan</artist>
		<country>USA</country>
		<company>Columbia</company>
		<price>10.90</price>
		<year>1985</year>
	</cd>
	<cd>
		<title>Empire Burlesque</title>
		<artirst>Bob Dylan</artist>
		<country>USA</country>
		<company>Columbia</company>
		<price>10.90</price>
		<year>1985</year>
	</cd>

</catalog>


<%@ Import Namespace="System.Data" %>

<script runat="server">
	sub Page_Load
		if Not Page.IsPostBack then
			dim mycdcatalog = New DataSet
			mycdcatalog.ReadXML(MapPath(""cdcatalog.xml))
		end if
	end sub
</script>
	

Then we create a Repeater control in .aspx page. The contents of the <HeaderTemplate> element are rendered first and only once within the output, then the contents of the <ItemTemplate> element are repeated for each "record" in the DataSet, and last, the contents of the <FooterTemplate> are rendered once within the output:



<html>
<body>

<form runat = "server">
	<asp:Repeater id = "cdcatalog" runat = "server">
		<HeaderTemplate>
		</HeaderTemplate>

		<ItemTemplate>
		</ItemTemplate>

		<FooterTemplate>
		</FooterTemplate>
	</asp:Repeater>
</form>
</body>
</html>


<%#Container.DataItem("fieldname")%>

	_Example
	<%@ Import Namespace="System.Data" %>
	<script runat ="server">
		sub Page_Load
		if Not Page.IsPostBack then
			dim mycdcatalog = New DataSet
			mycdcatalog.ReadXml(MapPath("cdcatalog.xml"))
			cdcatalog.DataSource = mycdcatalog
			cdcatalog.DataBind()
		end if
		end sub
	</script>
 

 	<html>
 	<body>

 		<form runat = "server">
 			<asp:Repeater id ="cdcatalog" runat="server">
 				<HeaderTemplate>
 					<table border="1" width="100%">
					<tr>
					<th>Title</th>
					<th>Artist</th>
					<th>Country</th>
					<th>Company</th>
					<th>Price</th>
					<th>Year</th>
					</tr>
 				</HeaderTemplate>
 				<ItemTemplate>
 					<tr>
					<td><%#Container.DataItem("title")%></td>
					<td><%#Container.DataItem("artist")%></td>
					<td><%#Container.DataItem("country")%></td>
					<td><%#Container.DataItem("company")%></td>
					<td><%#Container.DataItem("price")%></td>
					<td><%#Container.DataItem("year")%></td>
					</tr>
 				</ItemTemplate>
 				<FooterTemplate>
 					</table>
 				</FooterTemplate>
 				</asp:Repeater>
 		</form>
 	</body>
 	</html>


 	Using the <AlternatingItemTemplate> 

 	You can add an <AlternatingItemTemplate> element after the <ItemTemplate> element to describe the appearance of alternating rows of output. In the following example each other row in the table will be displayed in a light grey color:

 	_Example:


 	<%@ Import Namespace = "System.Data" %>

 	<script runat = "server">
 		sub Page_Load
 			if Not Page.IsPostBack then
 				dim mycdcatalog = new DataSet
 				mycdcatalog.ReadXml(MapPath("cdcatalog.xml"))
 				cdcatalog.DataSource = mycdcatalog
 				cdcatalog.DataBind()
 			end if 
 		end sub
 	</script>

	<html>
	<body>

	<form runat="server">
	<asp:Repeater id="cdcatalog" runat="server">

	<HeaderTemplate>
	<table border="1" width="100%">
	<tr>
	<th>Title</th>
	<th>Artist</th>
	<th>Country</th>
	<th>Company</th>
	<th>Price</th>
	<th>Year</th>
	</tr>
	</HeaderTemplate>

	<ItemTemplate>
	<tr>
	<td><%#Container.DataItem("title")%></td>
	<td><%#Container.DataItem("artist")%></td>
	<td><%#Container.DataItem("country")%></td>
	<td><%#Container.DataItem("company")%></td>
	<td><%#Container.DataItem("price")%></td>
	<td><%#Container.DataItem("year")%></td>
	</tr>
	</ItemTemplate>

	<AlternatingItemTemplate>
			<tr bgcolor = "#e8e8e8">
				<td><%#Container.DataItem("title")%></td>
				<td><%#Container.DataItem("artist")%></td>
				<td><%#Container.DataItem("country")%></td>
				<td><%#Container.DataItem("company")%></td>
				<td><%#Container.DataItem("price")%></td>
				<td><%#Container.DataItem("year")%></td>
			</tr>
	</AlternatingItemTemplate>

	<FooterTemplate>
	</FooterTemplate>
	</asp:Repeater>

	</form>
	</body>
	</html>



	Using The Separator Template

	The <SeparatorTemplate> element can be used to describe a separator between each record. The Following example inserts a horizontal line betweem each table row:


		<%@ Import Namespace="System.Data" %>

	<script  runat="server">
	sub Page_Load
	if Not Page.IsPostBack then
	   dim mycdcatalog=New DataSet
	   mycdcatalog.ReadXml(MapPath("cdcatalog.xml"))
	   cdcatalog.DataSource=mycdcatalog
	   cdcatalog.DataBind()
	end if
	end sub
	</script>

	<!DOCTYPE html>
	<html>
	<body>

	<form runat="server">
	<asp:Repeater id="cdcatalog" runat="server">

	<HeaderTemplate>
	<table border="0" width="100%">
	<tr>
	<th align="left">Title</th>
	<th align="left">Artist</th>
	<th align="left">Company</th>
	<th align="left">Price</th>
	</tr>
	</HeaderTemplate>

	<ItemTemplate>
	<tr>
	<td><%#Container.DataItem("title")%> </td>
	<td><%#Container.DataItem("artist")%> </td>
	<td><%#Container.DataItem("company")%> </td>
	<td><%#Container.DataItem("price")%> </td>
	</tr>
	</ItemTemplate>

	<SeparatorTemplate>
	<tr>
	<td colspan="6"><hr></td>
	</tr>
	</SeparatorTemplate>

	<FooterTemplate>
	</table>
	</FooterTemplate>

	</asp:Repeater>
	</form>

	</html>
	</body>


