ASP.NET Web Forms - The ArrayList Object

	The ArrayList object is a collection of items containing a single data value 


	ArrayList Radio Button List
			  Drop Down list

	<script runat = "server">
	Sub Page_Load
	if Not Page.IsPostBack then
		dim mycountries = new ArrayList()
		mycountries.Add("Norway") 
		myCountries.Add("Sweeden")
		myCountries.Add("France")
		mycountries.Add("Italy")

		mycountries.TrimToSize()
		rb.DataSource = myCountries
		rb.DataBind()
	end if
	End Sub

	sub displayMessage(s as Object, e As EventArgs)
		lbl1.text = "Your favourite country is " & rb.selectedItem.Text  
	end sub

	<form runat = "server">
		<asp:radioButtonList 
			id = "rb"
			runat = "server"
			AutoPostBack = "True"
			onSelectedIndexChanged = "displayMessage"
		>
		</asp:radioButtonList>
		<asp:DropDownList 
				id = "dd"
				runat = "server"
				AutoPostBack = "True"
				onSelectedIndexChanged = "displayMessage"
			>
			</asp:DropDownList>
		<p> asp:label id = "lbl1" runat="server"/>
		</p>
	</form>



By Default an arraylist contains 16 entries
but it can be trimmed to its real size by "TrimToSize()"



an ArrayList can be sort by the sort() Method

	myCountries.Sort()

To Sort it in reverse order
	
	myCountries.Reverse()


Data Binding To An Arraylist