What is Web Forms?

	Web Forms is one of the 3 programming models for creating ASP.NET web sites and web applications.

	The other two programming models are Web Pages and MVC (Model, View, Controller).

	Web Forms is the oldest ASP.NET programming model, with event driven web pages written as a combination of HTML, server controls, and server code.

	Web Forms are compiled and executed on the server, which generates the HTML that displays the web pages.

	Web Forms comes with hundreds of different web controls and web components to build user-driven web sites with data access.
