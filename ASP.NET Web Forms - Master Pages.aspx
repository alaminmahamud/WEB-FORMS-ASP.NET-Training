ASP.NET Web Forms - Master Pages 

Master pages Provide templates for other pages on your web site

Master pages

Master Pages allow you to create a consistent look and behaviour for all the pages (or group of pages) in your web app

MASTER PAGE Example

	<%@ Master %>
	<html>
	<body>
		<asp:ContentPlaceHolder id = "CPH1" runat = "server">
		</asp:ContentPlaceHolder>
	</body>
	</html>



	The master page above is a normal HTML page designed as a template for other pages.

	The @ Master directive defines it as a master page.

	The master page contains a placeholder tag <asp:ContentPlaceHolder> for individual content.

	The id="CPH1" attribute identifies the placeholder, allowing many placeholders in the same master page.

	This master page was saved with the name "master1.master".




Content Page Example

	<%@ Page MasterPageFile= "master1.master"%>

	<asp:Content ContentPlaceHolderId = "CPH1" runat="server">
	</asp:Content> 


	The content page above is one of the individual content pages of the web.

	The @ Page directive defines it as a standard content page.

	The content page contains a content tag <asp:Content> with a reference to the master page (ContentPlaceHolderId="CPH1").

	This content page was saved with the name "mypage1.aspx".

	When the user requests this page, ASP.NET merges the content page with the master page.


	Note:  The Content text must be inside the <asp:Conten> tag. No Content is allowed outside the tag.


Content Page With Controls

	<%@ Page MasterPageFile = "master1.master"%>
	<asp:Content ContentPlaceHolderId= "CPH1" runat= "server">
		<h2>
			Alamin Mahamud
		</h2>

		<form runat = "server">
			<asp:TextBox id ="textbox1" runat = "server"/>
			<asp:TextBox id = "textbox2" runat = "server"/>
		</form>
	</asp:Content>