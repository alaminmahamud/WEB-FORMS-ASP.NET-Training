You may save a lot of coding by maintaining the ViewState of the objects in your Web Form.

Maintaining the ViewState

	When a form is submitted in classic ASP, all form values are cleared. Suppose you have submitted a form with a lot of information and the server comes back with an error. You will have to go back to the form and correct the information. You click the back button, and what happens.......ALL form values are CLEARED, and you will have to start all over again! The site did not maintain your ViewState.

	When a form is submitted in ASP .NET, the form reappears in the browser window together with all form values. How come? This is because ASP .NET maintains your ViewState. The ViewState indicates the status of the page when submitted to the server. The status is defined through a hidden field placed on each page with a <form runat="server"> control. The source could look something like this:

	<form name="_ctl0" method="post" action="page.aspx" id="_ctl0">
	<input type="hidden" name="__VIEWSTATE"
	value="dDwtNTI0ODU5MDE1Ozs+ZBCF2ryjMpeVgUrY2eTj79HNl4Q=" />

	.....some code

	</form>
	Maintaining the ViewState is the default setting for ASP.NET Web Forms. If you want to NOT maintain the ViewState, include the directive <%@ Page EnableViewState="false" %> at the top of an .aspx page or add the attribute EnableViewState="false" to any control.

Look at the following .aspx file. It demonstrates the "old" way to do it. When you click on the submit button, the form value will disappear:

	<html>
		<body>
			<form action = "demo_classicasp.aspx" method = "post">
			Your name : <input type ="text" name = "fname" size = "20">
			    		<input type = "submit" value = "submit">
				
			    <%
			    	dim fname
			    	fname = Request.Form("fname")
			    	If fname<>"" Then
			    		Response.Write("Hello " & fname & "!")
			    	End If
			    %>

			</form>
		</body>
	</html>


Here is the New ASP.NET way. 
When you click on the submit button, the form value will not disappear''

	<script runat = "server">
		Sub submit(sender As Object, e As EventArgs)
			lbl1.Text = "Hello " & txt1.Text & "!";
		End Sub	
	</script>

	<html>
	<body>

	<form runat="server">
	Your name: <asp:TextBox id="txt1" runat="server" />
	<asp:Button OnClick="submit" Text="Submit" runat="server" />
	<p><asp:Label id="lbl1" runat="server" /></p>
	</form>

	</body>
	</html>
