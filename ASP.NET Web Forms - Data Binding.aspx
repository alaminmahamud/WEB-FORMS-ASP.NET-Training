ASP.NET Web Forms - Data Binding
	
	We may use data binding to fill lists with selectable items from an imported data source, like a database, an XML file, or a script.


Data Binding
The following controls are list controls which support data binding:

	asp:RadioButtonList
	asp:CheckBoxList
	asp:DropDownList
	asp:Listbox

The selectable items in each of the above controls are usually defined by one or more asp:ListItem controls, like this:

<form runat="server">
	<asp:RadioButtonList id="countrylist" runat="server"/>
	<asp:ListItem value = "N" text = "Norway"/>
	<asp:ListItem value = "S" text = "Sweden"/>
	<asp:ListItem value = "F" text = "France"/>
	<asp:ListItem value = "I" text = "Italy"/>
	</asp:RadioButtonList>
</form>