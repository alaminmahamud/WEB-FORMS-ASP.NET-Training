ASP.NET Web Forms - Server Controls

	Server controls are tags that are understood by the server.


Limitations in Classic ASP

	The listing below was copied from the previous chapter:

		<html>
		<body bgcolor="yellow">
		<center>
		<h2>Hello W3Schools!</h2>
		<p><%Response.Write(now())%></p>
		</center>
		</body>
		</html>
		The code above illustrates a limitation in Classic ASP: The code block has to be placed where you want the output to appear.

	With Classic ASP it is impossible to separate executable code from the HTML itself. This makes the page difficult to read, and difficult to maintain.



ASP.NET - Server Controls
	ASP.NET has solved the "spaghetti-code" problem described above with server controls.

	Server controls are tags that are understood by the server.
	There are three kinds of server controls:

		HTML Server Controls - Traditional HTML tags
		Web Server Controls - New ASP.NET tags
		Validation Server Controls - For input validation


ASP.NET Server Controls

	HTML server controls are HTML tags understood by the server.

	HTML elements in ASP.NET files are, by default, treated as text. To make these elements programmable, add a runat="server" attribute to the HTML element. This attribute indicates that the element should be treated as a server control. The id attribute is added to identify the server control. The id reference can be used to manipulate the server control at run time.

	Note: All HTML server controls must be within a <form> tag with the runat="server" attribute. The runat="server" attribute indicates that the form should be processed on the server. It also indicates that the enclosed controls can be accessed by server scripts.

	In the following example we declare an HtmlAnchor server control in an .aspx file. Then we manipulate the HRef attribute of the HtmlAnchor control in an event handler (an event handler is a subroutine that executes code for a given event). The Page_Load event is one of many events that ASP.NET understands:

	<script runat="server">
	
	Sub Page_Load
	link1.HRef="http://www.w3schools.com"
	End Sub

	</script>
	
	<html>
	<body>
	
		<form runat="server">
		<a id="link1" runat="server">Visit W3Schools!</a>
		</form>
	
	</body>
	</html>

The Executable COde itself has been moved outside the HTML



ASP.NET - Web Server Controls

	The Syntax for creating a web Server Control is : 

	<asp:control_name id="some_id" runat="server"/>

	<script runat = "server">
		Sub submit(Source As Object, e As EventArgs)
			button1.Text = "You Clicked Me!";
		End Sub
	</script>

	<form runat="server">
		<asp:Button id="button1" Text="Click Me!" runat="server" OnClick="submit"/>
	</form>



ASP.NET - Validation Server Controls

Validation server controls are used to validate user-input. If the user-input does not pass validation, it will display an error message to the user.

Each validation control performs a specific type of validation (like validating against a specific value or a range of values).

By default, page validation is performed when a Button, ImageButton, or LinkButton control is clicked. You can prevent validation when a button control is clicked by setting the CausesValidation property to false.

The syntax for creating a Validation server control is:

	<asp:control_name id="some_id" runat="server"/>

	EXAMPLE

	<html>
		<body>
			<form runat="server">
				<p>Enter a Number between 1 to 100:
				   <asp:TextBox id="tbox1" Text="Submit" runat="server" /><br/><br/>
				   <asp:Button Text="Submit" runat="server" />
				</p>

				<p>
					<asp:RangeValidator 
						ControlToValidate="tbox1"
						Minimum Value = "1"
						Maximum Value = "100"
						Type 		  = "Integer"
						Text 		  = "The Value Must be From 1 to 100!"
						runat		  = "server" 
					/>
				</p>
			</form>
		</body>
	</html>
