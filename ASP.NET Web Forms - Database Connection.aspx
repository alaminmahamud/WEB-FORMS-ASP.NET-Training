ASP.NET Web Forms - Database Connection

ADO.NET is also a part of the .NET Framework. ADO.NET is used to handle data access. With ADO.NET you can work with databases

ADO.NET is a part of the .NET Framework
ADO.NET consists of a set of classes used to handle data access
ADO.NET is entirely based on XML
ADO.NET has, unlike ADO, no Recordset object


CREATE A Database Connection

	<%@ Import Namespace = "System.Data.OleDb" %>

	<script runat = "server">
		sub Page_Load
			dim dbconn 
				dbconn = New OleDbConnection("Provider = Microsoft.Jet.OLEDB.4.0;data source = &server.mappath("northwind.mdb")")
				dbconn.Open()
		end sub
	</script>

	Note : The Connection string must be a continuous string without a line break

CREATE a DATABASE COmmand

	<%@ Import Namespace="System.Data.OleDb" %>

	<script runat="server">
	sub Page_Load
	dim dbconn,sql,dbcomm
	dbconn=New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;
	data source=" & server.mappath("northwind.mdb"))
	dbconn.Open()
	sql="SELECT * FROM customers"
	dbcomm=New OleDbCommand(sql,dbconn)
	end sub
	</script>

CREATE A DATAReader

	<%@ Import Namespace="System.Data.OleDb" %>

	<script runat="server">
	sub Page_Load
	dim dbconn,sql,dbcomm,dbread
	dbconn=New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;
	data source=" & server.mappath("northwind.mdb"))
	dbconn.Open()
	sql="SELECT * FROM customers"
	dbcomm=New OleDbCommand(sql,dbconn)

	dbread=dbcomm.ExecuteReader()

	end sub
	</script>

Bind to a Repeater Control
	
	<%@ Import Namespace="System.Data.OleDb" %>

	<script runat="server">
	sub Page_Load
	dim dbconn,sql,dbcomm,dbread
	dbconn=New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;
	data source=" & server.mappath("northwind.mdb"))
	dbconn.Open()
	sql="SELECT * FROM customers"
	dbcomm=New OleDbCommand(sql,dbconn)
	dbread=dbcomm.ExecuteReader()
	customers.DataSource=dbread
	customers.DataBind()
	dbread.Close()
	dbconn.Close()
	end sub
	</script>

	<html>
	<body>

	<form runat="server">
	<asp:Repeater id="customers" runat="server">

	<HeaderTemplate>
	<table border="1" width="100%">
	<tr>
	<th>Companyname</th>
	<th>Contactname</th>
	<th>Address</th>
	<th>City</th>
	</tr>
	</HeaderTemplate>

	<ItemTemplate>
	<tr>
	<td><%#Container.DataItem("companyname")%></td>
	<td><%#Container.DataItem("contactname")%></td>
	<td><%#Container.DataItem("address")%></td>
	<td><%#Container.DataItem("city")%></td>
	</tr>
	</ItemTemplate>

	<FooterTemplate>
	</table>
	</FooterTemplate>

	</asp:Repeater>
	</form>

	</body>
	</html>


Close the Database Connection
	
	dbread.Close()
	dbread.Close()


	

